#!/usr/bin/env python3

import os
import subprocess
import sys
import tempfile
import unittest
from os.path import basename

from subprocess import PIPE, STDOUT, DEVNULL

# Basename Test Case

class BasenameTestCase(unittest.TestCase):

    USAGE = 'Usage: mv [-v] [SOURCE] ... [DESTINATION]\n'    \
            '   -v  Cause mv to be verbose, showing files after they are moved\n'   

    file = open("test1.txt", "w") 
    file.write("Your text goes here") 
    file.close() 

    file = open("test2.txt", "w") 
    file.write("Your text goes here") 
    file.close() 

    file = open("test3.txt", "w") 
    file.write("Your text goes here") 
    file.close() 

    @classmethod
    def setUpClass(cls):
        print('\nTesting mv...', file=sys.stderr)

    def check_output(self, arguments, expect_failure=False):
        command = 'bin/idlebin mv {}'.format(arguments)
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT, shell=True)
        if expect_failure:
            self.assertNotEqual(process.returncode, 0)
        else:
            self.assertEqual(process.returncode, 0)

    def setUp(self):
        self.sandbox = tempfile.TemporaryDirectory()

    def tearDown(self):
        self.sandbox.cleanup()

    def check_valgrind(self, arguments):
        command = 'valgrind --leak-check=full bin/idlebin mv {}'.format(arguments)
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT, shell=True)
        lines = process.stdout.split(b'\n')
        errors = [int(l.split()[3]) for l in lines if b'ERROR SUMMARY' in l]

        self.assertEqual(process.returncode, 0)
        self.assertEqual(errors, [0])

    def test_00_help(self):
        command = 'bin/idlebin mv -h'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout.decode(), self.USAGE)

    def test_00_usage(self):
        command = 'bin/idlebin mv'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout.decode(), self.USAGE)

    def test_01_rename(self):
        with tempfile.NamedTemporaryFile(delete=False) as tf:
            self.check_output('{} test.txt'.format(tf.name))
            os.unlink('test.txt')

    def test_01_rename_error(self):
        self.check_output('kljlkj.txt test.txt', True)

    def test_01_rename_verbose(self):
        with tempfile.NamedTemporaryFile(delete=False) as tf:
            self.check_output('-v {} test.txt'.format(tf.name))
            os.unlink('test.txt')

    def test_01_rename_valgrind(self):
        with tempfile.NamedTemporaryFile(delete=False) as tf:
            self.check_valgrind('{} test.txt'.format(tf.name))

    def test_02_move(self):
        source = 'test1.txt'
        target = self.sandbox.name
        self.check_output('{} {}'.format(source, target))

    def test_02_move_error(self):
        source = 'fake.txt'
        self.check_output('{} lkajfdlk'.format(source), True)

    def test_02_move_verbose(self):
        source = 'test2.txt'
        target = os.path.join(self.sandbox.name, source)
        self.check_output('-v {} {}'.format(source, target))

    def test_02_move_valgrind(self):
        source = 'test3.txt'
        target = os.path.join(self.sandbox.name, source)
        self.check_output('{} {}'.format(source, target))

if __name__ == '__main__':
    unittest.main()
