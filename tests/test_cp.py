#!/usr/bin/env python3

import os
import stat
import subprocess
import sys
import tempfile
import unittest

from subprocess import PIPE, STDOUT

# Cp Test Case

class CpTestCase(unittest.TestCase):

    VALGRIND = 'valgrind --leak-check=full'
    USAGE    = b'Usage: cp [-Rv] SOURCE... TARGET\n'       \
               b'   -R    Copy directories recursively\n'  \
               b'   -v    Explain what is being done\n'
    PERMMASK = stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO

    @classmethod
    def setUpClass(cls):
        print('\nTesting cp...', file=sys.stderr)

    def execute(self, arguments, with_valgrind=False):
        command = 'bin/idlebin cp {}'.format(arguments)
        command = self.VALGRIND + ' ' + command if with_valgrind else command
        process = subprocess.run(command, stdout=PIPE, stderr=PIPE, shell=True)
        stderr  = process.stderr.split(b'\n')
        stderr  = b'\n'.join(l for l in stderr if not l.startswith(b'DEBUG'))
        return process.stdout, stderr, process.returncode

    def check_file(self, old_path, new_path=None):
        new_path = new_path or os.path.join(self.sandbox.name, os.path.basename(old_path))
        with open(old_path, 'rb') as old_fs, open(new_path, 'rb') as new_fs:
            self.assertEqual(old_fs.read(), new_fs.read())
        self.assertEqual(
            os.stat(old_path).st_mode | self.PERMMASK,
            os.stat(new_path).st_mode | self.PERMMASK
        )

    def check_valgrind(self, arguments):
        _, stderr, rc = self.execute(arguments, True)
        lines   = stderr.split(b'\n')
        errors  = [int(l.split()[3]) for l in lines if b'ERROR SUMMARY' in l]
        self.assertEqual(errors, [0])

    def setUp(self):
        self.sandbox = tempfile.TemporaryDirectory()

    def tearDown(self):
        self.sandbox.cleanup()

    def test_00_help(self):
        _, stderr, rc = self.execute('--help')
        self.assertEqual(stderr, self.USAGE)
        self.assertEqual(rc, 0)

    def test_01_usage(self):
        _, stderr, rc = self.execute('')
        self.assertEqual(stderr, self.USAGE)
        self.assertNotEqual(rc, 0)

    def test_02_one_argument(self):
        _, stderr, rc = self.execute('README.md')
        self.assertEqual(stderr, self.USAGE)
        self.assertNotEqual(rc, 0)

    def test_03_copy_file_to_new_file(self):
        source    = 'README.md'
        target    = os.path.join(self.sandbox.name, source)
        arguments = '{} {}'.format(source, target)
        _, stderr, rc  = self.execute(arguments)
        self.check_file(source)
        self.assertEqual(rc, 0)

    def test_03_copy_file_to_new_file_verbose(self):
        source    = 'README.md'
        target    = os.path.join(self.sandbox.name, source)
        arguments = '-v {} {}'.format(source, target)
        stdout, _, rc = self.execute(arguments)
        self.check_file(source)
        self.assertEqual(stdout.decode('utf-8'), '{} -> {}\n'.format(source, target))
        self.assertEqual(rc, 0)

    def test_03_copy_file_to_new_file_valgrind(self):
        source    = 'README.md'
        target    = os.path.join(self.sandbox.name, source)
        arguments = '{} {}'.format(source, target)
        self.check_valgrind(arguments)

    def test_04_copy_file_to_old_file(self):
        source    = 'README.md'
        target    = os.path.join(self.sandbox.name, source)
        arguments = '{} {}'.format(source, target)
        with open(target, 'w') as fs: pass  # Create file
        _, _, rc  = self.execute(arguments)
        self.check_file(source)
        self.assertEqual(rc, 0)

    def test_04_copy_file_to_old_file_verbose(self):
        source    = 'README.md'
        target    = os.path.join(self.sandbox.name, source)
        arguments = '-v {} {}'.format(source, target)
        with open(target, 'w') as fs: pass  # Create file
        stdout, _, rc  = self.execute(arguments)
        self.check_file(source)
        self.assertEqual(stdout.decode('utf-8'), '{} -> {}\n'.format(source, target))
        self.assertEqual(rc, 0)

    def test_04_copy_file_to_old_file_valgrind(self):
        source    = 'README.md'
        target    = os.path.join(self.sandbox.name, source)
        arguments = '{} {}'.format(source, target)
        with open(target, 'w') as fs: pass  # Create file
        self.check_valgrind(arguments)

    def test_05_copy_file_to_directory(self):
        sources   = ('README.md',)
        arguments = '{} {}'.format(' '.join(sources), self.sandbox.name)
        _, stderr, rc  = self.execute(arguments)
            #print(stderr)
        for source in sources:
            self.check_file(source)
        self.assertEqual(rc, 0)

    def test_05_copy_file_to_directory_verbose(self):
        sources   = ('README.md',)
        targets   = (os.path.join(self.sandbox.name, s) for s in sources)
        arguments = '-v {} {}'.format(' '.join(sources), self.sandbox.name)
        stdout, _, rc = self.execute(arguments)
        for source in sources:
            self.check_file(source)
        self.assertEqual(
            stdout.decode('utf-8'),
            ''.join('{} -> {}\n'.format(s, t) for s, t in zip(sources, targets))
        )
        self.assertEqual(rc, 0)

    def test_05_copy_file_to_directory_valgrind(self):
        sources   = ('README.md',)
        arguments = '{} {}'.format(' '.join(sources), self.sandbox.name)
        self.check_valgrind(arguments)

    def test_06_copy_files_to_directory(self):
        sources   = ('README.md', 'Makefile')
        arguments = '{} {}'.format(' '.join(sources), self.sandbox.name)
        _, _, rc  = self.execute(arguments)
        for source in sources:
            self.check_file(source)
        self.assertEqual(rc, 0)

    def test_06_copy_files_to_directory_verbose(self):
        sources   = ('README.md', 'Makefile')
        targets   = (os.path.join(self.sandbox.name, s) for s in sources)
        arguments = '-v {} {}'.format(' '.join(sources), self.sandbox.name)
        stdout, _, rc = self.execute(arguments)
        for source in sources:
            self.check_file(source)
        self.assertEqual(
            stdout.decode('utf-8'),
            ''.join('{} -> {}\n'.format(s, t) for s, t in zip(sources, targets))
        )
        self.assertEqual(rc, 0)

    def test_06_copy_files_to_directory_valgrind(self):
        sources   = ('README.md', 'Makefile')
        arguments = '{} {}'.format(' '.join(sources), self.sandbox.name)
        self.check_valgrind(arguments)

    def test_07_copy_DNE_to_directory(self):
        sources   = ('README.md', 'Makefile', '/DNE')
        arguments = '{} {}'.format(' '.join(sources), self.sandbox.name)
        _, stderr, rc = self.execute(arguments)
        for source in sources[:-1]:
            self.check_file(source)
        self.assertEqual(stderr, b'cp: cannot stat \'/DNE\': No such file or directory\n')
        self.assertNotEqual(rc, 0)

    def test_07_copy_DNE_to_directory_verbose(self):
        sources   = ('README.md', 'Makefile', '/DNE')
        targets   = (os.path.join(self.sandbox.name, s) for s in sources[:-1])
        arguments = '-v {} {}'.format(' '.join(sources), self.sandbox.name)
        stdout, stderr, rc = self.execute(arguments)
        for source in sources[:-1]:
            self.check_file(source)
        self.assertEqual(stderr, b'cp: cannot stat \'/DNE\': No such file or directory\n')
        self.assertEqual(
            stdout.decode('utf-8'),
            ''.join('{} -> {}\n'.format(s, t) for s, t in zip(sources, targets))
        )
        self.assertNotEqual(rc, 0)

    def test_07_copy_DNE_to_directory_valgrind(self):
        sources   = ('README.md', 'Makefile', '/DNE')
        arguments = '{} {}'.format(' '.join(sources), self.sandbox.name)
        self.check_valgrind(arguments)

    def test_08_copy_files_to_DNE(self):
        sources   = ('README.md', 'Makefile')
        arguments = '{} {}'.format(' '.join(sources), '/DNE')
        _, stderr, rc = self.execute(arguments)
        self.assertEqual(stderr, b'cp: target \'/DNE\' not a directory\n')
        self.assertNotEqual(rc, 0)

    def test_08_copy_files_to_DNE_verbose(self):
        sources   = ('README.md', 'Makefile')
        arguments = '-v {} {}'.format(' '.join(sources), '/DNE')
        stdout, stderr, rc = self.execute(arguments)
        self.assertEqual(stdout, b'')
        self.assertEqual(stderr, b'cp: target \'/DNE\' not a directory\n')
        self.assertNotEqual(rc, 0)

    def test_08_copy_files_to_DNE_valgrind(self):
        sources   = ('README.md', 'Makefile')
        arguments = '{} {}'.format(' '.join(sources), '/DNE')
        self.check_valgrind(arguments)

    def test_09_copy_directory_to_old_directory(self):
        sources   = ('bin', 'src')
        target    = os.path.join(self.sandbox.name)
        arguments = '-R {} {}'.format(' '.join(sources), target)
        _, _, rc  = self.execute(arguments)
        for source in sources:
            for root, dirs, files in os.walk(source):
                for name in dirs:
                    dpath = os.path.join(target, root, name)
                    self.assertTrue(os.path.exists(dpath))
                for name in files:
                    spath = os.path.join(root, name)
                    tpath = os.path.join(target, root, name)
                    self.check_file(spath, tpath)
        self.assertEqual(rc, 0)

    def test_09_copy_directory_to_old_directory_verbose(self):
        sources   = ('bin', 'src')
        target    = os.path.join(self.sandbox.name)
        arguments = '-Rv {} {}'.format(' '.join(sources), target)
        stdout, _, rc = self.execute(arguments)
        stdout    = sorted(l for l in stdout.decode('utf-8').split('\n') if l)
        expected  = []
        for source in sources:
            for root, dirs, files in os.walk(source):
                for name in sorted(files):
                    spath = os.path.join(root, name)
                    tpath = os.path.join(target, root, name)
                    expected.append('{} -> {}'.format(spath, tpath))
        self.assertEqual(stdout, sorted(expected))
        self.assertEqual(rc, 0)

    def test_09_copy_directory_to_old_directory_valgrind(self):
        sources   = ('bin', 'src')
        target    = os.path.join(self.sandbox.name)
        arguments = '-R {} {}'.format(' '.join(sources), target)
        self.check_valgrind(arguments)

    def test_10_copy_directory_to_new_directory(self):
        sources   = ('bin',)
        target    = os.path.join(self.sandbox.name, 'new')
        arguments = '-R {} {}'.format(' '.join(sources), target)
        _, _, rc  = self.execute(arguments)
        for source in sources:
            for root, dirs, files in os.walk(source):
                for name in dirs:
                    dpath = os.path.join(target, root.replace(source, ''), name)
                    self.assertTrue(os.path.exists(dpath))
                for name in files:
                    spath = os.path.join(root, name)
                    tpath = os.path.join(target, root.replace(source, ''), name)
                    self.check_file(spath, tpath)
        self.assertEqual(rc, 0)

    def test_10_copy_directory_to_new_directory_verbose(self):
        sources   = ('bin',)
        target    = os.path.join(self.sandbox.name, 'new')
        arguments = '-Rv {} {}'.format(' '.join(sources), target)
        stdout, _, rc = self.execute(arguments)
        stdout    = sorted(l for l in stdout.decode('utf-8').split('\n') if l)
        expected  = []
        for source in sources:
            for root, dirs, files in os.walk(source):
                for name in sorted(files):
                    spath = os.path.join(root, name)
                    tpath = os.path.join(target, root.replace(source, ''), name)
                    expected.append('{} -> {}'.format(spath, tpath))
        self.assertEqual(stdout, expected)
        self.assertEqual(rc, 0)

    def test_10_copy_directory_to_new_directory_valgrind(self):
        sources   = ('bin',)
        target    = os.path.join(self.sandbox.name, 'new')
        arguments = '-R {} {}'.format(' '.join(sources), target)
        self.check_valgrind(arguments)

# Main Execution

if __name__ == '__main__':
    unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
