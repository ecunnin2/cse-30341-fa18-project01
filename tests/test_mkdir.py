#!/usr/bin/env python3

import os
import subprocess
import sys
from pathlib import Path
import unittest

from subprocess import PIPE, STDOUT

# RM Test Case

class MKDIRTestCase(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        print('\nTesting mkdir...', file=sys.stderr)
        os.environ['LC_ALL'] = 'C'
    
    def check_output(self, arguments, expect_error=False):
        src_cmd = 'bin/idlebin mkdir {}'.format(arguments)
        src_prc = subprocess.run(src_cmd, stdout=PIPE, stderr=STDOUT, shell=True)
        src_out = src_prc.stdout
        
        tgt_cmd = '/bin/mkdir {}'.format(arguments)
        tgt_prc = subprocess.run(tgt_cmd, stdout=PIPE, stderr=STDOUT, shell=True)
        tgt_out = tgt_prc.stdout
        
        if expect_error:
            src_out = src_out.split(b':', 1)[-1].replace(b"'", b"")
            tgt_out = tgt_out.split(b':', 1)[-1].replace(b"'", b"")
        
        if expect_error:
            self.assertNotEqual(src_prc.returncode, 0)
            self.assertNotEqual(tgt_prc.returncode, 0)
        else:
            self.assertEqual(src_prc.returncode, 0)
            self.assertEqual(tgt_prc.returncode, 0)

    def check_valgrind(self, arguments, expect_error=False):
        command = 'valgrind --leak-check=full bin/idlebin mkdir {}'.format(arguments)
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT, shell=True)
        lines   = process.stdout.split(b'\n')
        errors  = [int(l.split()[3]) for l in lines if b'ERROR SUMMARY' in l]
    
        self.assertEqual(errors, [0])
        if expect_error:
            self.assertNotEqual(process.returncode, 0)
        else:
            self.assertEqual(process.returncode, 0)

    def test_00_help(self):
        command = 'bin/idlebin mkdir --help'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout, b'Usage: mkdir [-v] DIRECTORY...\n')

    def test_01_mkdir_directory(self):
        command = 'bin/idlebin mkdir tempDir'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(os.path.isdir('tempDir'), True)
        command = 'rm tempDir'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)

# Main Execution

if __name__ == '__main__':
    unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:

