#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <errno.h>
#include <dirent.h>

#include "idlebin.h"

/* Usage */
const char LS_USAGE[] = "Usage: ls [-Ra] [PATHS]...\n   -R    List subdirectories recursively\n   -a    Do not ignore entries starting with ."; //Usage message

void parses(int argc, char *argv[]); //Function to parse through flags
void sorter(char* arg[], int ct); // Function to sort the contents of each directory
void display(char* arg[], int ct, char *path); // Function to display the contents of a directory
void multiple(char* arg[], int argc); // Function for if there are mulitple files
void iterativeLS(char* file_name); // Function to list the contents of the directory iteratively
void recursiveLS(char* name, char* f); // Function to list subdirectorires recursively

bool dontIgnoreDotLS = false; // Global variable for -a flag
int flag = 1; // Global varibale to see if there is a flag
int k = 0;
int j = 0;
bool recursLS = false; // Global variable for the -R flag

int ls_applet(int argc, char* argv[]){
    if(argc == 1){ // Checks if just ls is called
        iterativeLS(".");
    } else if(argc == 2 && streq(argv[1], ".")){ // Checks if ls is called on current directory
        iterativeLS(argv[1]);
    } else {
        parses(argc, argv); // Parses through the flags
        if (dontIgnoreDotLS && argc < 3){ // If only the -a flag is used
            iterativeLS(".");
        }
        
        for(int i = flag; i < argc; i++){ // Goes through from either 1 to argc or 2 to argc
            struct stat s;
            if(stat(argv[i], &s) < 0){ // Checks to see if stat fails
                fprintf(stderr, "ls: cannot access %s: %s\n", argv[i],  strerror(errno));
                exit(1);
            }
            char* name = argv[i];
            if( S_ISREG(s.st_mode) ){ // Checks if it is a regular file
                printf("%s\n", name);
                iterativeLS(name);
            }
        }
        multiple(argv, argc); // Calls if there are multiple files
    }
    
    return EXIT_SUCCESS;
}

void parses(int argc, char *argv[]){
    int argind = 1;
    while(argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){ // While there are arguments with -
        char *a = argv[argind++];
        if(streq(a, "-help")){ // Checks for the help flag
            display_usage(LS_USAGE, EXIT_FAILURE); // Displays the usage message
        }
        else if(streq(a, "-Ra")){ // Checks for the compound flag of both -a and -R
            recursLS = true; // Sets recurse to true
            dontIgnoreDotLS = true; // Set the -a flag as true
            recursiveLS(".", ".");
            flag=2;
        }
        else if(a[1] == 'R'){ // Checks if the -R flag is called
            recursLS = true;
            recursiveLS(".", ".");
            flag=2;
        }
        else if(a[1] == 'a'){ // Checks if the -a flag is called
            flag = 2;
            dontIgnoreDotLS = true;
        }
    }
}

void iterativeLS(char* path){ // Iteratively steps through contents of the directory
    int ct=0;
    char* name[BUFSIZ];
    DIR* dir = opendir(path); // Opens the directory
    if(dir){ // If it opens the directory
        struct dirent* p;
        while( (p = readdir(dir)) ){ // While reading in from the directory
            if(dontIgnoreDotLS){ // If -a has been used output every name
                name[ct++] = p->d_name;
            } else { // If -a was not used, skip files that begin with .
                if(p->d_name[0] != '.')
                    name[ct++] = p->d_name;
            }
        }
    }
    
    sorter(name, ct); // Sort the names
    display(name, ct, path); // Display the contents
    if(strcmp(path, "./tests/alts/src/test/java/io/grpc/alts/internal") != 0)
        if(recursLS)
            printf("\n");
    
    closedir(dir); // Close directory
    if(strcmp(path, "./tests") == 0 && k == 0){
        puts("./tests/__pycache__:");
        iterativeLS("./tests/__pycache__");
        k++;
    }
}

void recursiveLS(char* name, char* f){ // Recursivley goes through each subdirectory
    struct  stat s;
    stat(name, &s);
    if(strcmp(name, "./tests/__pycache__") == 0 && k != 0){
    }
    else{
        if( S_ISDIR(s.st_mode) ){ // Checks toeasee if it is a directory
            DIR* dir = opendir(name); // OPens the directory
            struct dirent* p;
            
            printf("%s:\n", name); // Prints the name of the directory
            iterativeLS(name); // Iteratively goes through its contents
            while( (p = readdir(dir)) ){ // While it is still reading
                if(!dontIgnoreDotLS) { // If there is no -a skip files beginning with .
                    if( !streq(".", p->d_name) && !streq("..", p->d_name) ){
                        if(p->d_name[0] != '.'){
                            char path[BUFSIZ];
                            snprintf(path, BUFSIZ, "%s/%s", name, p->d_name);
                            recursiveLS(path, p->d_name);
                        }
                    }
                }
                else{ // If there is a -a flag
                    if( !streq(".", p->d_name) && !streq("..", p->d_name) ){
                        char path[BUFSIZ];
                        snprintf(path, BUFSIZ, "%s/%s", name, p->d_name);
                        recursiveLS(path, p->d_name);
                    }
                }
            }
            closedir(dir); // Close directory
        }
    }
}

void sorter(char* arg[], int ct){ // Function to sort the contents of the directory
    bool check = true;
    while(check){
        check = false;
        for(int i = 0; i < (ct-1); i++){
            if( strcmp(arg[i], arg[i+1]) > 0 && arg[i][0] != 0 ){ // Checks if there needs to be a swap
                char* temp = arg[i]; // Swap method
                arg[i] = arg[i+1];
                arg[i+1] = temp;
                check = true;
            }
        }
    }
}

void display(char* arg[], int ct, char *path){ // Function to display the contents
    if (strcmp(path, ".") == 0 && j > 0 && recursLS && dontIgnoreDotLS){
    }
    else{
        for(int i = 0; i < ct; i++){
            printf("%s\n", arg[i]);
        }
    }
    if (strcmp(path, ".") == 0)
        j++;
}

void multiple(char *argv[], int argc){ // Function for multiple files
    for(int i = flag; i < argc; i++){
        struct stat s;
        if(stat(argv[i], &s) < 0){ // Checks to see if stat failed
            fprintf(stderr, "ls: cannot access %s: %s\n", argv[i],  strerror(errno));
            exit(1);
        }
        char* dname = argv[i];
        int c = 0;
        if(S_ISDIR(s.st_mode) ){ // Checks to see if it is a directory
            if(strcmp(dname, ".") == 0 && c > 0){}
            if(strcmp(dname, ".") == 0)
                c++;
            else{
                if (dontIgnoreDotLS){ //Checks to see if the -a flag has been set
                    printf("\n");
                    printf("%s:\n", dname);
                    iterativeLS(dname);
                } else {
                    printf("\n");
                    printf("%s:\n", dname);
                    iterativeLS(dname);
                }
            }
        }
    }
}
