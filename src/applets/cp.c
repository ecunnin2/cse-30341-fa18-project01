
/* Title: cp.c
  * Author(s): Edward Cunningham, Kieren DiGiorno
  * Publishing Date: Sept. 7, 2018
  * Description: A simple program to copy one file, several files, a directory
  * or a combination of the sort to a new destination. If the destination is a
  * file, then only one file can be a source.
  *
  * Copywrite 2018
**/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <dirent.h>
#include <sys/types.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

/* Logging Macros */

#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...)   printf("[%5d] DEBUG %10s:%-4d " M "\n", getpid(), __FILE__, __LINE__, ##__VA_ARGS__)
#endif

const char CP_USAGE[] = "Usage: cp [-Rv] SOURCE... TARGET\n   -R    Copy directories recursively\n   -v    Explain what is being done";

/* Function Declarations */
void cp_usage(const char *progname, int status);
bool parse(int argc, char *argv[]);
int scanRec(char *path, char *dest);
int scan(char *path, char *dest);
int execute(int sourcesCount, char *sources[], char *path);
bool copyFile(char *src, char *dest);
void checkCompoundFlagsCP(char *programName, char *flag);
bool makeDir(char *path);


/* Global Variables */
bool realVerbose    = false;
bool verbose        = false;
bool recursiveCP    = false;

/******* Function Implementations********/

/**
 * The usage function.
 * */
void cp_usage(const char *progname, int status) {
    fprintf(stderr, "Usage: cp [-Rv] SOURCE... TARGET\n");
    fprintf(stderr, "   -R    Copy directories recursively\n");
    fprintf(stderr, "   -v    Explain what is being done\n");
    exit(status);
}

/**
 * The main function. Parses out the options and arguments
 * including the sources and destinations, as well as the
 * flags. Then a function, execute() is called that will 
 * do some logic andn trigger subsequint functions.
 * */
int cp_applet(int argc, char *argv[]) {
    /* Parse command-line options */
    parse(argc, argv);;
    
    char *destination = argv[argc - 1];

    int sourcesCount = 0;
    char *sources[255];

    for (int i=1; i<argc - 1; i++) {
        if (argv[i][0] != '-') {
            sources[sourcesCount] = argv[i];
            sourcesCount++;
        }
    }

    return execute(sourcesCount, sources, destination);
}

/**
 * Parse out the flags, list of source files (and/or) 
 * destination file/directory.
 * */
bool parse(int argc, char *argv[]){
    
    if (argc < 3) {
        cp_usage(argv[0], 1);
    }

    if (argc == 3 && argv[1][0] =='-') {
        cp_usage(argv[0], 1);
    }
    int argind = 1;
    
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
        char *arg = argv[argind++];
        checkCompoundFlagsCP(argv[0], arg);
        switch(arg[1]){
            case 'R':
                recursiveCP = true;
                break;
            case 'a':
                verbose = true;
                break;
            case 'v':
                realVerbose = true;
                break;
            default:
                cp_usage(argv[0], 1);
                break;
        }
    }
    return true;
}

/**
 *
 * */
void checkCompoundFlagsCP(char *programName, char *flag){
    if (strlen(flag) == 3) {
        for (int i=1; i<strlen(flag); i++) {
            switch(flag[i]){
                case 'R':
                    recursiveCP = true;
                    break;
                case 'v':
                    verbose = true;
                    break;
                case 'a':
                    verbose = true;
                    break;
                default:
                    cp_usage(programName, 1);
                    break;
            }
        }
    } else {
        return;
    }
}


/**
 * Check to see if a directory exists, and if it does not
 * then make it.
 * */
bool makeDir(char *dest){
    struct stat dt = {0};
    if (stat(dest, &dt) == -1) {
        if (mkdir(dest, 0700) != 0) {
            if (verbose) {
                debug("Unable to make directory: %s", dest);
            }
            return false;
        }
        if (verbose) {
            debug("Making new directory: %s", dest);
        }
    } else {
        if (verbose) {
            debug("Directory already exists: %s", dest);
        }
    }
    return true;
}



/**
 *
 * */
int execute(int count, char *sources[], char *dest){
    struct stat s, t;
        if (count == 1 && (stat(dest, &s) >= 0) && (stat(sources[0], &t) >= 0)) {
            if (S_ISDIR(s.st_mode) && !S_ISDIR(t.st_mode)) {
                // Copy file to directory
                if (verbose) {
                    debug("Copying file to directory");
                }
                char fullDestination[255] = "";
                strcat(fullDestination, dest);
                    
                if (fullDestination[strlen(fullDestination) - 1] != '/') {
                    strcat(fullDestination, "/");
                }
                strcat(fullDestination, sources[0]);
                copyFile(sources[0], fullDestination);
            } else if (!S_ISDIR(s.st_mode) && !S_ISDIR(t.st_mode)) {
                // Copy file to file
                if (verbose) {
                    debug("Copying file to file");
                }
                copyFile(sources[0], dest);
            } else if (S_ISDIR(s.st_mode) && S_ISDIR(t.st_mode)) {
                char fullDestination[255] = "";
                strcat(fullDestination, dest);
                
                if (fullDestination[strlen(fullDestination) - 1] != '/') {
                    strcat(fullDestination, "/");
                }
                strcat(fullDestination, sources[0]);
                
                if (verbose) {
                    debug("copying directory: %s to %s", sources[0], fullDestination);
                }
                if (recursiveCP) {
                    makeDir(fullDestination);
                    scanRec(sources[0], fullDestination);
                } else {
                    makeDir(fullDestination);
                    scan(sources[0], fullDestination);
                }
        }
    } else if (count == 1) {
        if (stat(sources[0], &s) != 0) {
            fprintf(stderr, "cp: cannot stat \'%s\': No such file or directory\n", sources[0]);
            exit(1);
        }
        
        if (S_ISDIR(s.st_mode) && (stat(dest, &t) == 0)) {
            char fullDestination[255] = "";
            strcat(fullDestination, dest);
            
            if (fullDestination[strlen(fullDestination) - 1] != '/') {
                strcat(fullDestination, "/");
            }
            strcat(fullDestination, sources[0]);
            
            if (verbose) {
                debug("copying directory: %s to %s", sources[0], fullDestination);
            }
            if (recursiveCP) {
                makeDir(fullDestination);
                scanRec(sources[0], fullDestination);
            } else {
                makeDir(fullDestination);
                scan(sources[0], fullDestination);
            }
        } else if (S_ISDIR(s.st_mode) && (stat(dest, &t) != 0)) {
            if (verbose) {
                debug("copying directory: %s to %s", sources[0], dest);
            }
            if (recursiveCP) {
                makeDir(dest);
                scanRec(sources[0], dest);
            } else {
                makeDir(dest);
                scan(sources[0], dest);
            }
        } else {
            copyFile(sources[0], dest);
        }
    } else if (count > 1) {
        struct stat t;
        if (stat(dest, &t) != 0) {
            fprintf(stderr, "cp: target \'%s\' not a directory\n", dest);
            exit(1);
        }
        
        // Copy multiple files to directory
        for (int i=0; i<count; i++) {
            if (stat(sources[i], &t) != 0) {
                fprintf(stderr, "cp: cannot stat \'%s\': No such file or directory\n", sources[i]);
                exit(1);
            }
            
            char fullDestination[255] = "";
            strcat(fullDestination, dest);
            
            if (fullDestination[strlen(fullDestination) - 1] != '/') {
                strcat(fullDestination, "/");
            }
            strcat(fullDestination, sources[i]);
            
            struct stat s;
            if (stat(sources[i], &s) >= 0) {
                if (S_ISDIR(s.st_mode) && recursiveCP) {
                    // Copy a dir to dir recursively
                    if (verbose) {
                        debug("Copying dir rec: %s to %s", sources[i], fullDestination);
                    }
                    makeDir(fullDestination);
                    scanRec(sources[i], fullDestination);
                } else if (S_ISDIR(s.st_mode) && !recursiveCP) {
                    if (verbose) {
                        debug("Copying directory: %s to %s", sources[i], fullDestination);
                    }
                    makeDir(fullDestination);
                    scan(sources[i], fullDestination);
                } else if (S_ISREG(s.st_mode)) {
                    // Copy a file to dir
                    if (verbose) {
                        debug("Copying file: %s to %s", sources[i], fullDestination);
                    }
                    copyFile(sources[i], fullDestination);
                }
            } else if (stat(dest, &s) < 0) {
                fprintf(stderr, "cp: target \'%s\' not a directory\n", dest);
                exit(1);
            }
        }
    }
    
    return 0;
}

/**
 * Very similar to scan(char *path) except when a sub-directory
 * is reached during the scan, then call scanRec on that sub-directory.
 * This is to copy all files and sub-directories in a directory.
 * */
int scanRec(char *path, char *dest){
    DIR  *d = opendir(path);
    
    if (d == NULL) {
        // Directory doesn't exist
        fprintf(stderr, "cp: target '%s' not a directory\n", path);
        exit(1);
    }
    
    if (verbose) {
        debug("Scanning %s", path);
    }
    
    struct dirent *e;
    while ((e = readdir(d))) {
        if (strcmp(e->d_name, ".") == 0 || strcmp(e->d_name, "..") == 0) {
            continue;
        }
        
        char fullSource[255] = "";
        strcat(fullSource, path);
        
        if (fullSource[strlen(fullSource) - 1] != '/') {
            strcat(fullSource, "/");
        }
        strcat(fullSource, e->d_name);
        
        char fullDestination[255] = "";
        strcat(fullDestination, dest);
        if (fullDestination[strlen(fullDestination) - 1] != '/') {
            strcat(fullDestination, "/");
        }
        strcat(fullDestination, e->d_name);
        
        if (e->d_type == DT_DIR) {
            makeDir(fullDestination);
            scanRec(fullSource, fullDestination);
        } else if (e->d_type == DT_REG) {
            copyFile(fullSource, fullDestination);
        }
    }
    closedir(d);
    return EXIT_SUCCESS;
}


/**
 * Iterate through a directory. For each element, if it is
 * a directory, then make a copy of that directory in the new
 * location. If it is a file then simply copy the file over.
 * */
int scan(char *path, char *dest){
    DIR  *d = opendir(path);
    
    if (d == NULL) {
        // Directory doesn't exist
        fprintf(stderr, "cp: target '%s' not a directory\n", path);
        exit(1);
    }
    
    if (verbose) {
        debug("Scanning %s", path);
    }
    
    struct dirent *e;
    while ((e = readdir(d))) {
        if (strcmp(e->d_name, ".") == 0 || strcmp(e->d_name, "..") == 0) {
            continue;
        }
        char fullSource[255] = "";
        strcat(fullSource, path);
        
        if (fullSource[strlen(fullSource) - 1] != '/') {
            strcat(fullSource, "/");
        }
        strcat(fullSource, e->d_name);
        
        char fullDestination[255] = "";
        strcat(fullDestination, dest);
        if (fullDestination[strlen(fullDestination) - 1] != '/') {
            strcat(fullDestination, "/");
        }
        strcat(fullDestination, e->d_name);
        copyFile(fullSource, fullDestination);
    }
    closedir(d);
    return EXIT_SUCCESS;
}

/**
 * The actual function to copy a file. This opens the source and destination
 * files then loads the source file bytes into a buffer and writes that buffer
 * into the destination file. Both files are then closed.
 * */
bool copyFile(char *src, char *dest){
    /* Open source file for reading */
    if (verbose) {
        debug("Opening source file %s for reading", src);
    }
    int rfd = open(src, O_RDONLY);
    if (rfd < 0) {
        fprintf(stderr, "cp: cannot stat \'%s\': No such file or directory\n", src);
        exit(1);
    }
    
    struct stat s;
    if (stat(src, &s) < 0){
        fprintf(stderr, "cp: cannot stat \'%s\': No such file or directory\n", src);
        close(rfd);
        exit(1);
    }
    
    /* Open destination file for writing */
    if (verbose) {
    debug("Opening destination file, %s for writing", dest);
    }
    int wfd = open(dest, O_CREAT|O_WRONLY, s.st_mode);    // Add mode
    if (wfd < 0) {
        fprintf(stderr, "cp: target \'%s\' not a file\n", dest);
        close(rfd);
        exit(1);
    }
    
    /* Copy from source to destination */
    char buffer[BUFSIZ];
    int  nread;
    if (verbose) {
    debug("Writing to file");
    }
    while ((nread = read(rfd, buffer, BUFSIZ)) > 0) {
        int nwritten = write(wfd, buffer, nread);
        while (nwritten != nread) {                 // Write in loop
            nwritten += write(wfd, buffer + nwritten, nread - nwritten);
        }
    }

        printf("%s -> %s\n", src, dest);
    /* Cleanup */
    close(rfd);
    close(wfd);

    return true;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */

