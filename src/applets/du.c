/* Title: du.c
 * Author(s): Edward Cunningham
 * Publishing Date: Sept. 7, 2018
 * Description: A simple program to list the disk usage for a given directory.
 *
 * Copywrite 2018
 **/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>


/* Function declarations */
int compoundSize(char *file);
void interatively(char *path);
void recursively(char *path);
void DU_USAGE(const char *progname, int status);
void displayInformation(int total, char *directory);
void checkCompoundFlags(char *programName, char *flag);
bool parseFlags(int argc, char *argv[]);

/* Global Variables */
bool recursive = false;
bool dontIgnoreDot = false;

bool parseFlags(int argc, char *argv[]){
    // Check for --help.
    if (argc >= 2 && (strcmp(argv[1], "--help") == 0)) {
        DU_USAGE(argv[0], 0);
    }
    
    int argind = 1;
    
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
        char *arg = argv[argind++];
        checkCompoundFlags(argv[0], arg);
        switch(arg[1]){
            case 'R':
                recursive = true;
                break;
            case 'a':
                dontIgnoreDot = true;
                break;
            default:
                DU_USAGE(argv[0], 1);
                break;
        }
    }
    return true;
}

void checkCompoundFlags(char *programName, char *flag){
    if (strlen(flag) == 3) {
        for (int i=1; i<strlen(flag); i++) {
            switch(flag[i]){
                case 'R':
                    recursive = true;
                    break;
                case 'a':
                    dontIgnoreDot = true;
                    break;
                default:
                    DU_USAGE(programName, 1);
                    break;
            }
        }
    } else {
        return;
    }
}

void displayInformation(int total, char *directory){
    printf("%d", total);
    printf("\t");
    printf( "%s",directory);
    printf("\n");
}

int du_applet(int argc, char *argv[]) {
    char *path = ".";
    parseFlags(argc, argv);
    if (recursive) {
        recursively(path);
    } else {
       interatively(path);
    }
}

void DU_USAGE(const char *progname, int status) {
    fprintf(stderr, "Usage: %s [-Ra] [PATHS]...\n", progname);
    fprintf(stderr, "    -R            List subdirectories recursively\n");
    fprintf(stderr, "    -a            Do not ignore entries starting with .\n");
    exit(status);
}

int compoundSize(char *filename){
    struct stat st;
    stat(filename, &st);
    return st.st_size;
}

void interatively(char *path){
    int size = 0;
    DIR  *d    = opendir(path);
    
    if (d == NULL) {
        fprintf(stderr, "Unable to opendir %s: %s\n", path, strerror(errno));
        return;
    }
    
    struct dirent *e;
    while ((e = readdir(d))) {
        
        if ((e->d_name[0] == '.') && dontIgnoreDot) {
            continue;
        }
        
        size = size + compoundSize(e->d_name);
        
    }
    
    closedir(d);
    displayInformation((size/1000), path);
}

void recursively(char *path){
    int size = 0;
    
    DIR  *d    = opendir(path);
    
    if (d == NULL) {
        fprintf(stderr, "Unable to opendir %s: %s\n", path, strerror(errno));
        return;
    }
    
    struct dirent *e;
    while ((e = readdir(d))) {
        
        if ((e->d_name[0] == '.') && dontIgnoreDot) {
            continue;
        }
        
        if ((e->d_type == DT_DIR) && (strcmp(".", e->d_name) != 0 && strcmp("..", e->d_name) != 0)) {
            char pathName[255] = "";
            strcat(pathName, path);
            strcat(pathName, "/");
            strcat(pathName, e->d_name);
            recursively(pathName);
        }
        size = size + compoundSize(e->d_name);
    }
    
    closedir(d);
    displayInformation((size), path);
}
/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */

