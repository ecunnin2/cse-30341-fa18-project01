/* yes.c: yes applet */

#include "idlebin.h"

/* Usage */

const char YES_USAGE[] = "Usage: yes [STRING]...";

/* Applet */

int yes_applet(int argc, char *argv[]) {
    char message[BUFSIZ] = "y";

    if (argc > 1) {
        strncpy(message, argv[1], BUFSIZ);
        for (int i = 2; i < argc; i++) {
            strcat(message, " ");
            strcat(message, argv[i]);
        }
    }

    while (1) {
        puts(message);
    }

    return EXIT_FAILURE;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
