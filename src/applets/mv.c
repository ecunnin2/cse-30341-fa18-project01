#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <stdbool.h>

bool parse_flags(int argc, char *arv[]); // Function to parse through flags
void MV_USAGE(const char *progname, int status); // Function to display the usage

bool verboseMV = false;

int mv_applet(int argc, char *argv[]){
    if (argc < 2){ // Checks to ensure there are enough arguments
        MV_USAGE(argv[0], 1);
    }
    parse_flags(argc, argv); // Parses the flags
    char *src;
    char *dst;
    if(verboseMV){ // If the verbose flag is set, get source and destination from 3rd and 4th arguments
        src = argv[2];
        dst = argv[3];
    }
    else{ // Get destination and source normally
        src = argv[1];
        dst = argv[2];
    }
    
    struct stat s;

    if(stat(dst, &s) || !S_ISDIR(s.st_mode)){} // Checks if it is a file
    else{ // If it is a directory make a new path for the file
        char newpath[255] = "";
        strcat(newpath, dst);
        strcat(newpath, "/");
        strcat(newpath, src);
        dst = newpath;
    }
    int sfd = open(src, O_RDONLY); // Open the source file
    if (sfd < 0){ // Checks if open failed
        fprintf(stderr, "Unable to open %s: %s\n", src, strerror(errno));
        return EXIT_FAILURE;
    }
    if (stat(src, &s) < 0){ // Check if stat failed
        fprintf(stderr, "stat failed\n");
        close(sfd);
        return EXIT_FAILURE;
    }
    int dfd = open(dst, O_CREAT|O_WRONLY, s.st_mode); // Open destination file or new path
    if (dfd < 0){
        fprintf(stderr, "Unable to open %s: %s\n", dst, strerror(errno));
        close(sfd);
        return EXIT_FAILURE;
    }

    char buffer[BUFSIZ];
    int nread;

    while((nread = read(sfd, buffer, BUFSIZ)) > 0){ // Write to the new destination from the source
        int nwritten = write(dfd, buffer, nread);
        while (nwritten != nread){
            nwritten += write(dfd, buffer + nwritten, nread - nwritten);
        }
    }
    if(verboseMV){ // Print out the path if the verbose flag is set
        printf("%s -> %s\n", src, dst);
    }
    close(sfd); // Close the source
    close(dfd); // Close the destination
    unlink(src); // Remove the original file
    return EXIT_SUCCESS;
}

void MV_USAGE(const char *progname, int status){ // Function to display the usage
    fprintf(stderr, "Usage: %s [-v] [SOURCE] ... [DESTINATION]\n", progname);
    fprintf(stderr, "   -v  Cause mv to be verbose, showing files after they are moved\n");
    exit(status);
}

bool parse_flags(int argc, char *argv[]){ // Parse through the flags
    if (argc >= 2 && (strcmp(argv[1], "-h") == 0)){
        MV_USAGE(argv[0], 0);
    }
    int argind = 1;
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
        char *arg = argv[argind++];
        switch(arg[1]){
            case 'v':
                verboseMV = true;
                break;
            default:
                MV_USAGE(argv[0], 1);
                break;
        }
    }
    return true;
}
