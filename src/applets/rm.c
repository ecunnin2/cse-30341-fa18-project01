
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <dirent.h>
#include <sys/types.h>
#include <idlebin.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

/* Logging Macros */


/** Global Variables **/
const char RM_USAGE[] = "Usage: rm [-Rv] DIRECTORY...";
bool rm_verbose = false;

/** Function Declarations **/
char *parse_rm(int argc, char *argv[]);
void current_removeDir(char *dest);
void rm_usage(const char *progname, int status);

/** Main Function **/
int rm_applet(int argc, char *argv[]) {
    
    char *path = parse_rm(argc, argv);
    current_removeDir(path);
    
    return EXIT_SUCCESS;
}

void rm_usage(const char *progname, int status) {
    fprintf(stderr, "%s", RM_USAGE);
    exit(status);
}

/**
 * Check to see if a directory exists, and if it does not
 * then make it.
 * */
void current_removeDir(char *dest){
    struct stat directory;
    if (stat(dest, &directory) != 0) {
        fprintf(stderr, "rm: cannot remove \‘%s\’: No such file or directory\n", dest);
        exit(1);
    } else {
        DIR  *d = opendir(dest);
        
        if (d == NULL) {
            // Directory doesn't exist
            fprintf(stderr, "rm: cannot remove \‘%s\’: No such file or directory\n", dest);
            exit(1);
        }
        
        struct dirent *e;
        while ((e = readdir(d))) {
            if (strcmp(e->d_name, ".") == 0 || strcmp(e->d_name, "..") == 0) {
                continue;
            }
            
            char fullSource[255] = "";
            strcat(fullSource, dest);
            
            if (fullSource[strlen(fullSource) - 1] != '/') {
                strcat(fullSource, "/");
            }
            strcat(fullSource, e->d_name);
            struct stat stat_entry;
            // stat for the entry
            stat(fullSource, &stat_entry);
            
            // recursively remove a nested directory
            if (S_ISDIR(stat_entry.st_mode) != 0) {
                current_removeDir(fullSource);
                continue;
            }
            
            // remove a file object
            if (unlink(fullSource) == 0)
                printf("Removed a file: %s\n", fullSource);
            else
                printf("Can`t remove a file: %s\n", fullSource);
        }
        // remove the devastated directory and close the object of it
        if (rmdir(dest) == 0)
            printf("Removed a directory: %s\n", dest);
        else
            printf("Can`t remove a directory: %s\n", dest);
        
        closedir(d);
    }
}

/**
 * Parse out the flags, return the destination file/directory.
 * */
char *parse_rm(int argc, char *argv[]){
    int argind = 1;
    
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
        char *arg = argv[argind++];
        
        switch(arg[1]){
            case 'V':
                rm_verbose = true;
                break;
            default:
                rm_usage(argv[0], 1);
                break;
        }
    }
    for (int i = 1; i < argc; i++) {
        if (argv[i][0] != '-') {
            return argv[i];
        }
    }
    return "";
}



