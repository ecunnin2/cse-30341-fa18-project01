#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <dirent.h>
#include <sys/types.h>
#include <idlebin.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

/* Logging Macros */


/** Global Variables **/
const char MKDIR_USAGE[] = "Usage: mkdir [-v] DIRECTORY...";
bool mkdir_verbose = false;

/** Function Declarations **/
char *parse_mkdir(int argc, char *argv[]);
bool current_makeDir(char *dest);
void mkdir_usage(const char *progname, int status);


/** Main Function **/
int mkdir_applet(int argc, char *argv[]) {
    
    char *path = parse_mkdir(argc, argv);
    current_makeDir(path);
    
    return EXIT_SUCCESS;
}

void mkdir_usage(const char *progname, int status) {
    fprintf(stderr, "%s", MKDIR_USAGE);
    exit(status);
}

/**
 * Check to see if a directory exists, and if it does not
 * then make it.
 * */
bool current_makeDir(char *dest){
    struct stat dt = {0};
    if (stat(dest, &dt) == -1) {
        if (mkdir(dest, 0700) != 0) {
            fprintf(stderr, "mkdir: cannot create directory \‘%s\’: No such file or directory\n", dest);
            return false;
        }
    }
    return true;
}

/**
 * Parse out the flags, return the destination file/directory.
 * */
char *parse_mkdir(int argc, char *argv[]){
    int argind = 1;
    
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
        char *arg = argv[argind++];

        switch(arg[1]){
            case 'V':
                mkdir_verbose = true;
                break;
            default:
                mkdir_usage(argv[0], 1);
                break;
        }
    }
    for (int i = 1; i < argc; i++) {
        if (argv[i][0] != '-') {
            return argv[i];
        }
    }
    return "";
}


