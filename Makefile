# Configuration

CC		= gcc
LD		= gcc
CFLAGS		= -g -std=gnu99 -Wall -Iinclude
LDFLAGS		= -Llib

# Variables

APPLETS_SOURCES = $(wildcard src/applets/*.c)
APPLETS_OBJECTS	= $(APPLETS_SOURCES:.c=.o)
IDLEBIN_SOURCES = src/idlebin.c src/idlebin_applets.c
IDLEBIN_OBJECTS = $(IDLEBIN_SOURCES:.c=.o)
IDLEBIN_LINKS   = $(subst src/applets,bin,$(basename $(APPLETS_OBJECTS)))
IDLEBIN_PROGRAM = bin/idlebin

# Rules

all:	$(IDLEBIN_PROGRAM) $(IDLEBIN_LINKS)

%.o:		%.c include/idlebin.h
	@echo "Compiling $@"
	@$(CC) $(CFLAGS) -c -o $@ $<

$(IDLEBIN_PROGRAM):		$(APPLETS_OBJECTS) $(IDLEBIN_OBJECTS)
	@echo "Linking $@"
	@$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

$(IDLEBIN_LINKS): 		$(IDLEBIN_PROGRAM)
	@echo "Symlinking $@"
	@ln -s idlebin $@

src/idlebin_applets.c:		$(APPLETS_SOURCES)
	@echo "Generating $@"
	@printf "#include \"idlebin.h\"\n" > $@
	@gawk 'match($$0, /int\s+(.*)_applet\(int.*, char.*\)/, m) {\
		print "extern const char " toupper(m[1]) "_USAGE[];"\
	    }' $^ >> $@
	@printf "Applet APPLETS[] = {\n" >> $@
	@gawk 'match($$0, /int\s+(.*)_applet\(int.*, char.*\)/, m) {\
		print "{\"" m[1] "\"," toupper(m[1]) "_USAGE," m[1] "_applet},"\
	    }' $^ >> $@
	@printf "{NULL, NULL}\n};\n" >> $@

include/idlebin.h:		include/idlebin_applets.h

include/idlebin_applets.h: 	$(APPLETS_SOURCES)
	@echo "Generating $@"
	@printf "#ifndef IDLEBIN_APPLETS_H\n#define IDLEBIN_APPLETS_H\n" > $@
	@gawk 'match($$0, /(int.*_applet\(int.*, char.*\))/, m) {print m[1] ";"}' $^ >> $@
	@printf "#endif" >> $@

test:	$(IDLEBIN_PROGRAM)
	@python3 -m unittest discover tests -v

clean:
	@echo "Removing applet objects"
	@rm -f $(APPLETS_OBJECTS)

	@echo "Removing $(IDLEBIN_PROGRAM)"
	@rm -f $(IDLEBIN_PROGRAM)

	@echo "Removing symlinks"
	@rm -f $(IDLEBIN_LINKS)
