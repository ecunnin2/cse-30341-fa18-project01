# Project 01: Idlebin

This is [Project 01] of [CSE-30341-FA18].

## Members

- Kieran DiGiorno (kdigiorn@nd.edu)
- Eddie Cunningham (ecunnin2@nd.edu)

## Demonstration

[https://docs.google.com/presentation/d/1dEbV3wg8AOQl4ykx8v43eqGVbVry0k026y4k-ni-0_4/edit?usp=sharing]()
- [ ] `uname [-asnrvmo]` (**3**)

## Applets

### Group

- [X] `ls [-Ra] [PATHS]...` (**6**)
    This applet works completely and passes all of the tests except -Ra. This
    seems tp be due to the fact that there are extra subdirectories being added
    with our implementation.

- [X] `cp [-Rv] SOURCE... TARGET` (**6**)
    

### Individual

- [X] `mkdir -v [DIRECTORY]...` (**3**) -- Eddie Cunningham

- [X] `rmdir -v [DIRECTORY]...` (**3**) -- Eddie Cunningham

- [X] `mv [-v] SOURCE ... DESTINATION` (**6**) -- Kieran DiGiorno


[Project 01]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa18/project01.html
[CSE-30341-FA18]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa18/
