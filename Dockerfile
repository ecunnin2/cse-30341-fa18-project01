# To Build: docker build --no-cache -t pbui/cse-30341-fa18-project01 . < Dockerfile

FROM	    ubuntu:18.04
MAINTAINER  Peter Bui <pbui@nd.edu>

RUN	    apt update -y

# Run-time dependencies
RUN	    apt install -y build-essential python3 gawk valgrind
